import { Response } from 'express';
export declare class AuthController {
    constructor();
    steamAuth(req: any, res: Response): Promise<void>;
    validateSteamAuth(req: any, res: any): Promise<void>;
}
