import { VerifyCallback } from "passport-steam";
declare const SteamStrategy_base: new (...args: any[]) => any;
export declare class SteamStrategy extends SteamStrategy_base {
    constructor();
    validate(identifier: any, profile: any, done: VerifyCallback): Promise<any>;
}
export {};
