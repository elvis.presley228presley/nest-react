import { AppService } from './app.service';
export declare class AppController {
    private readonly appService;
    constructor(appService: AppService);
    steamAuth(req: any): Promise<void>;
    steamAuthRedirect(req: any): any;
}
