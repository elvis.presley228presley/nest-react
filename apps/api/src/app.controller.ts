import {Controller, Get, Req, UseGuards} from '@nestjs/common';
import { AppService } from './app.service';
import {AuthGuard} from "@nestjs/passport";

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('steam')
  @UseGuards(AuthGuard('steam'))
  async steamAuth(@Req() req) {

    console.log('steamAuth');
  }

  @Get('/auth/steam/return')
  @UseGuards(AuthGuard('steam'))
  steamAuthRedirect(@Req() req) {
    console.log('steamAuthRedirect', req.user);
    return req.user;
  }

}
