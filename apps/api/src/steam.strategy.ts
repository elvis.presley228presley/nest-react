// import { Injectable } from '@nestjs/common';
// import { PassportStrategy } from '@nestjs/passport';
// import { Strategy } from 'passport-steam';
//
// @Injectable()
// export class SteamStrategy extends PassportStrategy(Strategy, 'steam') {
//     constructor() {
//         super({
//             returnURL: 'http://localhost:3000/auth/steam/return',
//             realm: 'http://localhost:3000/',
//             apiKey: '471F6D51D7DC72B1206BF287DC6C34F7',
//         });
//     }
//
//     async validate(identifier: string, profile: any, done: any): Promise<any> {
//
//         return profile.user;
//     }
// }


import {Injectable} from "@nestjs/common";
import {Strategy, VerifyCallback} from "passport-steam";
import {PassportStrategy} from "@nestjs/passport";

@Injectable()
export class SteamStrategy extends PassportStrategy(Strategy, 'steam') {
    constructor() {
        super({
            returnURL: 'http://react-vite-test.ru/api/auth/steam/return',
            realm: 'http://react-vite-test.ru/',
            apiKey: 'A45C3501CE2FC503F974A70130B5F325',
        });
    }
    async validate(identifier, profile, done: VerifyCallback): Promise<any> {
        const { personaname, profileurl, avatarfull, realname, loccountrycode } = profile._json;
        console.log(`profile._json`, profile._json)
        const { displayName } = profile;

        const user = {
            name: personaname,
            profile: profileurl,
            avatar: avatarfull,
            realName: realname,
            country: loccountrycode,
            displayName: displayName,
            identifier,
        }
        console.log(`user`, user)
        done(null, profile)
    }
}