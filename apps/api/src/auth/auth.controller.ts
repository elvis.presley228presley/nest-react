import {
    Controller,
    Get,
    Req,
    Res,
    UseGuards,
} from '@nestjs/common';
import { Response } from 'express';
import {SteamAuthGuard} from "./guards/steam-auth.guard";

@Controller('auth')
export class AuthController {
    constructor() {}

    @Get('steam')
    @UseGuards(SteamAuthGuard)
    async steamAuth(@Req() req: any, @Res() res: Response) {
    }

    @Get('steam/return')
    @UseGuards(SteamAuthGuard)
    async validateSteamAuth(
        @Req() req: any,
        @Res() res: any,
    ) {
        req.user;
    }
}
