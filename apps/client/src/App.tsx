import './App.css'
import axios from "axios";

function App() {

  const handleClick = async () => {
    try {
      const response = await axios.get('api/steam');
      console.log('response :', response);

      window.location.href = response.request.responseURL;
    } catch (error) {
      console.error('Ошибка авторизации через Steam:', error);
    }
  };

  return (
      <div
          onClick={handleClick}
      >
        войти через steam
      </div>
  )
}

export default App
